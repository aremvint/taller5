

struct producto;
//******DECLARACIONES PRIMER EJERCICIO
void bytesInt(int valor);
void bytesLong(long valor);
void bytesFloat(float valor);
void bytesDouble(double valor);

//******DECLARACIONES SEGUNDO EJERCICIO
void datosURL(char *url);

//******DECLARACIONES TERCER EJERCICIO
void NumOcurrencias(int *array,int valor);

//******DECLARACIONES CUARTO EJERCICIO
void imc(float peso, float altura, float **resultado);

//******DECLARACIONES QUINTO EJERCICIO


struct producto
{
	char nombre[20];
	float precio;
	int stock;
	float precioIVA;
	char SKU[20];
};
void iva(struct producto *p);

//******DECLARACIONES SEXTO EJERCICIO
//void direcciones(lista *p, int arreglo);
