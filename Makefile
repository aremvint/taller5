all: taller5
taller5: main.o ejercicios.o
	gcc -Wall -I -lm ./obj/*.o -o ./bin/taller5

main.o: src/main.c ./include/funciones.h
	gcc -Wall -c -I. -lm ./src/main.c -o ./obj/main.o

ejercicios.o: src/ejercicios.c ./include/funciones.h
	gcc -Wall -c -I. -lm ./src/ejercicios.c -o ./obj/ejercicios.o

clean: 
	rm -rf ./obj/*.o
	echo "Archivos .o eliminados"











